﻿using Quartz;
using System.Threading.Tasks;

namespace GitlabTest.Jobs
{
    /// <summary>
    ///
    /// </summary>
    [DisallowConcurrentExecution]
    public class HelloJob : IJob
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                System.Console.WriteLine("测试任务运行中......");
            });
        }
    }
}